﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Requests
{
    public class UsersListRequest
    {
        [JsonProperty("filter")]
        public string Filter { get; set; }
    }
}
