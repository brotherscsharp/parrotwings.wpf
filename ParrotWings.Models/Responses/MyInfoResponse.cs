﻿using ParrotWings.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class MyInfoResponse : BaseResponse
    {
        public FullUserInfo Info { get; set; }
    }
}
