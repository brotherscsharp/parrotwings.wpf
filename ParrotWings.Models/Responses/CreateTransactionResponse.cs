﻿using ParrotWings.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class CreateTransactionResponse : BaseResponse
    {
        public Transaction Transactions { get; set; }
    }
}
