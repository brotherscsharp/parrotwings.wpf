﻿using ParrotWings.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class TransactionsResponse : BaseResponse
    {
        public List<Transaction> Transactions { get; set; }
    }
}
