﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class SignInUpResponse : BaseResponse
    {
        public string id_token { get; set; }
    }
}
