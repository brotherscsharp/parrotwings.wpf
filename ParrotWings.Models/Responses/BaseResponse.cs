﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class BaseResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}
