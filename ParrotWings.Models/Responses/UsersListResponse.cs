﻿using ParrotWings.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Responses
{
    public class UsersListResponse : BaseResponse
    {
        public List<UserInfo> Users { get; set; }
    }
}
