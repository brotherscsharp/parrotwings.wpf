﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Models
{
    public class UserInfo
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
