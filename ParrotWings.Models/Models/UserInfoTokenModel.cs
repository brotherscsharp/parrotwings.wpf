﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Models
{
    public partial class UserInfoTokenModel
    {
        [JsonProperty("user_info_token", Required = Required.Always)]
        public FullUserInfo UserInfoToken { get; set; }
    }
}
