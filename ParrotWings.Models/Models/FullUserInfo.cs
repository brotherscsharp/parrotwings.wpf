﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Models
{
    public class FullUserInfo : UserInfo
    {

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}
