﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Models
{
    public partial class CreateTransactionTokenModel
    {
        [JsonProperty("trans_token", Required = Required.Always)]
        public Transaction Transaction { get; set; }
    }
}
