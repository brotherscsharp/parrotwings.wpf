﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Models.Models
{
    public partial class TransactionTokenModel
    {
        [JsonProperty("trans_token", Required = Required.Always)]
        public List<Transaction> Transactions { get; set; }
    }
}
