﻿using Flurl.Http;
using Newtonsoft.Json;
using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using ParrotWings.Services.Interfaces;
using Polly;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.Services
{
    public class AuthService : IAuthService
    {
        public async Task<SignInUpResponse> Login(LoginRequest request)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/sessions/create";

                using var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                using var client = new HttpClient();
                var result = await client.PostAsync(url, content);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    var resObject = JsonConvert.DeserializeObject<SignInUpResponse>(json);
                    resObject.IsSuccess = true;
                    return resObject;
                }

                return new SignInUpResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }

        public async Task<SignInUpResponse> Register(RegistrationRequest request)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/users";

                using var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                using var client = new HttpClient();
                var result = await client.PostAsync(url, content);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    var resObject = JsonConvert.DeserializeObject<SignInUpResponse>(json);
                    resObject.IsSuccess = true;
                    return resObject;
                }

                return new SignInUpResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }
    }
}
