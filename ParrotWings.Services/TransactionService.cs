﻿using Newtonsoft.Json;
using ParrotWings.Models.Models;
using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using ParrotWings.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.Services
{
    public class TransactionService : ITransactionService
    {
        public async Task<CreateTransactionResponse> CreateTransaction(CreateTransactionRequest request, string accessToken)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/api/protected/transactions";

                using var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var result = await client.PostAsync(url, content);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var resObject = JsonConvert.DeserializeObject<CreateTransactionTokenModel>(json);
                    var myInfo = new CreateTransactionResponse
                    {
                        Transactions = resObject.Transaction,
                        IsSuccess = true,
                    };
                    return myInfo;
                }

                return new CreateTransactionResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }

        public async Task<TransactionsResponse> GetTransactions(string accessToken)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/api/protected/transactions";

                using var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var result = await client.GetAsync(url);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var resObject = JsonConvert.DeserializeObject<TransactionTokenModel>(json);
                    var myInfo = new TransactionsResponse
                    {
                        Transactions = resObject.Transactions,
                        IsSuccess = true,
                    };
                    return myInfo;
                }

                return new TransactionsResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }
    }
}
