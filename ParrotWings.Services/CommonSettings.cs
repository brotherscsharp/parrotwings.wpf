﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.Services
{
    public class CommonSettings
    {
        public const string BASE_URL = "http://193.124.114.46:3001";

        public static readonly int NumberOfRetries = 1;
        public static TimeSpan DefaultTimeout = TimeSpan.FromSeconds(10);
        public static TimeSpan SleepDurationProvider(int retry) => TimeSpan.FromSeconds(Math.Pow(2, retry));
        public static TimeSpan SleepDurationProviderLogin(int retry) => TimeSpan.FromSeconds(Math.Pow(2, retry));
    }
}
