﻿using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using System.Threading.Tasks;

namespace ParrotWings.Services.Interfaces
{
    public interface IAuthService
    {
        Task<SignInUpResponse> Register(RegistrationRequest request);
        Task<SignInUpResponse> Login(LoginRequest request);
    }
}
