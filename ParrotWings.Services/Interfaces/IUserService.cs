﻿using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.Services.Interfaces
{
    public interface IUserService
    {
        Task<MyInfoResponse> MyInfo(string accessToken);
        Task<UsersListResponse> GetUsersList(string accessToken, UsersListRequest filter);
    }
}
