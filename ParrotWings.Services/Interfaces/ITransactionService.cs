﻿using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.Services.Interfaces
{
    public interface ITransactionService
    {
        Task<TransactionsResponse> GetTransactions(string accessToken);
        Task<CreateTransactionResponse> CreateTransaction(CreateTransactionRequest request, string accessToken);
    }
}
