﻿using Newtonsoft.Json;
using ParrotWings.Models.Models;
using ParrotWings.Models.Requests;
using ParrotWings.Models.Responses;
using ParrotWings.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.Services
{
    public class UserService : IUserService
    {
        public async Task<UsersListResponse> GetUsersList(string accessToken, UsersListRequest filter)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/api/protected/users/list";

                using var content = new StringContent(JsonConvert.SerializeObject(filter), Encoding.UTF8, "application/json");
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var result = await client.PostAsync(url, content);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var resObject = JsonConvert.DeserializeObject<List<UserInfo>>(json);
                    var myInfo = new UsersListResponse
                    {
                        Users = resObject,
                        IsSuccess = true,
                    };
                    return myInfo;
                }

                return new UsersListResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }

        public async Task<MyInfoResponse> MyInfo(string accessToken)
        {
            try
            {
                var url = $"{CommonSettings.BASE_URL}/api/protected/user-info";

                using var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var result = await client.GetAsync(url);
                var json = await result.Content.ReadAsStringAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var resObject = JsonConvert.DeserializeObject<UserInfoTokenModel>(json);
                    var myInfo = new MyInfoResponse
                    {
                         Info = resObject.UserInfoToken,
                         IsSuccess = true,
                    };
                    return myInfo;
                }

                return new MyInfoResponse
                {
                    IsSuccess = false,
                    Message = json
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                throw (ex);
            }
        }
    }
}
