﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF.Classes
{    
    public class ChangeViewEvent : PubSubEvent<string> { }
    public class UpdateBalanceEvent : PubSubEvent { }
    
}
