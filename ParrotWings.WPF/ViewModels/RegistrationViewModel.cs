﻿using Notification.Wpf;
using ParrotWings.Models.Requests;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Helpers;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.WPF.ViewModels
{
    public class RegistrationViewModel : BaseViewModel
    {
        private readonly IAuthService _authService;
        public RegistrationViewModel(IAppService appService, IAuthService authService) : base(appService)
        {
            _authService = authService;

            // Demo Data
            Email = MockData.Email;
            Name = MockData.UserName;
            Password = MockData.Password;
        }

        private DelegateCommand _registerCommand;
        public DelegateCommand RegisterCommand => _registerCommand ??= new DelegateCommand(async () => await Register());

        private DelegateCommand _gotoLoginCommand;
        public DelegateCommand GotoLoginCommand => _gotoLoginCommand ??= new DelegateCommand(GotoLogin);


        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        private async Task Register()
        {
            if (!Valid()) return;
            try
            {
                IsBusy = true;
                await Task.Delay(1000);
                var res = await _authService.Register(new RegistrationRequest
                {
                    Email = Email,
                    Password = Password,
                    UserName = Name,
                });

                if (!res.IsSuccess)
                {
                    NotificationManager.Show(res.Message, NotificationType.Error);
                    return;
                }
                NotificationManager.Show($"Sign up succeeded", NotificationType.Success);
                Properties.Settings.Default.AccessToken = res.id_token;
                Properties.Settings.Default.Save();

                EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.Transactions);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Register: {ex.Message} {ex.StackTrace}");
                NotificationManager.Show("Something went wrong", NotificationType.Error);
            } 
            finally
            {
                IsBusy = false;
            }
        }

        private void GotoLogin()
        {
            EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.Login);
        }

        private bool Valid()
        {
            if (!ValidationHelper.IsValidEmail(Email))
            {
                NotificationManager.Show("Enter valid email", NotificationType.Warning);
                return false;
            }

            if (!ValidationHelper.IsValidUserName(Name))
            {
                NotificationManager.Show("Name should contains more then 2 symbols", NotificationType.Warning);
                return false;
            }

            if (!ValidationHelper.IsValidPassword(Password))
            {
                NotificationManager.Show("Password should contains more then 4 symbols", NotificationType.Warning);
                return false;
            }

            return true;
        }
    }
}
