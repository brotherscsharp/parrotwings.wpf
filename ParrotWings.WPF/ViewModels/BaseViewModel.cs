﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Notification.Wpf;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Serilog;

namespace ParrotWings.WPF.ViewModels
{
    public abstract class BaseViewModel : BindableBase, INavigationAware, IDisposable
    {
        private IRegionNavigationService? _regionNavigationService;

        private CancellationTokenSource? _navigationCts;
        private DelegateCommand? _goBackCommand;
        private bool _isBusy;

        protected BaseViewModel(IAppService appService)
        {
            RegionManager = appService.RegionManager;
            EventAggregator = appService.EventAggregator;
            NotificationManager = appService.NotificationManager;
            // Logger = appService.Logger;
        }

        protected ILogger Logger { get; }
        protected IRegionManager RegionManager { get; }
        protected IEventAggregator EventAggregator { get; }
        protected INotificationManager NotificationManager { get; }

        protected CancellationToken NavigationCancellationToken => _navigationCts?.Token ?? new CancellationToken(true);

        public virtual void OnNavigatedTo(NavigationContext navigationContext)
        {

            _regionNavigationService = navigationContext.NavigationService;
            _navigationCts = new CancellationTokenSource();

            GoBackCommand.RaiseCanExecuteChanged();
        }

        public virtual bool IsNavigationTarget(NavigationContext navigationContext) => true;

        public virtual void OnNavigatedFrom(NavigationContext navigationContext)
        {

            if (_navigationCts?.IsCancellationRequested == false)
                _navigationCts.Cancel();

            GoBackCommand.RaiseCanExecuteChanged();

        }

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }


        public DelegateCommand GoBackCommand => _goBackCommand ??= new DelegateCommand(OnGoBack, CanGoBack);

        protected virtual void OnGoBack() => _regionNavigationService!.Journal.GoBack();
        protected virtual bool CanGoBack() => _regionNavigationService?.Journal.CanGoBack ?? false;

        public virtual void Dispose()
        {
            if (_navigationCts?.IsCancellationRequested == false)
            {
                _navigationCts.Cancel();
                _navigationCts.Dispose();
            }
        }
    }
}
