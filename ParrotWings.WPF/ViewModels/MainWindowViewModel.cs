﻿using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using System;

namespace ParrotWings.WPF.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        private string _title = MockData.AppName;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel(IAppService appService) : base(appService)
        {
            EventAggregator.GetEvent<ChangeViewEvent>().Subscribe((view) => NavigeteToView(view));
        }

        private DelegateCommand _pageLoadedCommang;
        public DelegateCommand PageLoadedCommang => _pageLoadedCommang ??= new DelegateCommand(PageLoaded);

        private void NavigeteToView(string view)
        {
            if (view == ViewNames.Transactions)
            {
                RegionManager.RequestNavigate(RegionNames.UserInfo, ViewNames.UserInfo);
            }
            RegionManager.RequestNavigate(RegionNames.MainContent, view);
        }

        private void PageLoaded()
        {
            RegionManager.RequestNavigate(RegionNames.Header, ViewNames.Header);
            RegionManager.RequestNavigate(RegionNames.MainContent, ViewNames.Login);
            // RegionManager.RequestNavigate(RegionNames.UserInfo, ViewNames.UserInfo);
        }
    }
}
