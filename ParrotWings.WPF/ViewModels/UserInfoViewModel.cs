﻿using ParrotWings.Models.Models;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.WPF.ViewModels
{
    public class UserInfoViewModel : BaseViewModel
    {
        private readonly IUserService _userService;
        public UserInfoViewModel(IAppService appService, IUserService userService) : base(appService)
        {
            _userService = userService;
            EventAggregator.GetEvent<UpdateBalanceEvent>().Subscribe(async () => await LoadPageData());
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            LoadPageData().GetAwaiter();

            // Demo Data
            UserInfo = MockData.UserInfo;
        }

        private FullUserInfo _userInfo;

        public FullUserInfo UserInfo
        {
            get => _userInfo;
            set => SetProperty(ref _userInfo, value);
        }

        private async Task LoadPageData()
        {
            var myInfo = await _userService.MyInfo(Properties.Settings.Default.AccessToken);
            if(myInfo.IsSuccess)
            {
                UserInfo = myInfo.Info;
            }
        }
    }
}
