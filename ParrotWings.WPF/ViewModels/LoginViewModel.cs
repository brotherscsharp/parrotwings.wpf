﻿using Notification.Wpf;
using ParrotWings.Models.Requests;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Helpers;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.WPF.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IAuthService _authService;
        public LoginViewModel(IAppService appService, IAuthService authService) : base(appService)
        {
            _authService = authService;

            // demo data
            Email = MockData.Email;
            Password = MockData.Password;
        }

        private DelegateCommand _loginCommand;
        public DelegateCommand LoginCommand => _loginCommand ??= new DelegateCommand(async() => await Login());

        private DelegateCommand _gotoRegisterCommand;
        public DelegateCommand GotoRegisterCommand => _gotoRegisterCommand ??= new DelegateCommand(GotoRegister);

        private string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        private async Task Login()
        {
            if (!Valid()) return;
            try
            {
                IsBusy = true;
                await Task.Delay(1000);
                var res = await _authService.Login(new LoginRequest
                {
                    Email = Email,
                    Password = Password,
                });

                if (!res.IsSuccess)
                {
                    NotificationManager.Show(res.Message, NotificationType.Error);
                    return;
                }
                NotificationManager.Show($"Sign in succeeded", NotificationType.Success);
                Properties.Settings.Default.AccessToken = res.id_token;
                Properties.Settings.Default.Save();

                EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.Transactions);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Register: {ex.Message} {ex.StackTrace}");
                NotificationManager.Show("Something went wrong", NotificationType.Error);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void GotoRegister()
        {
            EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.Registration);
        }

        private bool Valid()
        {
            if (!ValidationHelper.IsValidEmail(Email))
            {
                NotificationManager.Show("Enter valid email", NotificationType.Warning);
                return false;
            }

            if (!ValidationHelper.IsValidPassword(Password))
            {
                NotificationManager.Show("Password should contains more then 4 symbols", NotificationType.Warning);
                return false;
            }

            return true;
        }
    }
}
