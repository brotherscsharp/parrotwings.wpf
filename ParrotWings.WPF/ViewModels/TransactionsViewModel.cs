﻿using ParrotWings.Models.Models;
using ParrotWings.Models.Requests;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace ParrotWings.WPF.ViewModels
{
    public class TransactionsViewModel : BaseViewModel
    {
        private readonly ITransactionService _transactionServic;

        public TransactionsViewModel(IAppService appService, ITransactionService transactionService) : base(appService)
        {
            _transactionServic = transactionService;

            // Demo data
            UserTransactions = MockData.Transactions;
        }

        private DelegateCommand _sendFunsCommand;
        public DelegateCommand SendFunsCommand => _sendFunsCommand ??= new DelegateCommand(SendFuns);
                

        private ObservableCollection<Transaction> _userTransactions;

        public ObservableCollection<Transaction> UserTransactions
        {
            get => _userTransactions;
            set => SetProperty(ref _userTransactions, value);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
                        
            LoadPageData().GetAwaiter();
        }

        private async Task LoadPageData()
        {
            var transactionResult = await _transactionServic.GetTransactions(Properties.Settings.Default.AccessToken);
            if (transactionResult.IsSuccess)
            {
                UserTransactions = new ObservableCollection<Transaction>(transactionResult.Transactions);
                EventAggregator.GetEvent<UpdateBalanceEvent>().Publish();
            }
        }

        private void SendFuns()
        {
            EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.CreateTransaction);
        }
    }
}
