﻿using Notification.Wpf;
using ParrotWings.Models.Models;
using ParrotWings.Models.Requests;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Classes;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Commands;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace ParrotWings.WPF.ViewModels
{
    public class CreateTransactionViewModel : BaseViewModel
    {
        private readonly IUserService _userService;
        private readonly ITransactionService _transactionService;

        public CreateTransactionViewModel(IAppService appService, IUserService userService, ITransactionService transactionService) : base(appService)
        {
            _userService = userService;
            _transactionService = transactionService;
            Users = MockData.Users;
        }

        private DelegateCommand _sendFundsCommand;
        public DelegateCommand SendFundsCommand => _sendFundsCommand ??= new DelegateCommand(async() => await SendFunds());

        private DelegateCommand _cancelCommand;
        public DelegateCommand CancelCommand => _cancelCommand ??= new DelegateCommand(BackToTransactions);


        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                _ = GetSuggestions();
            }
        }

        private ObservableCollection<UserInfo> _users;
        public ObservableCollection<UserInfo> Users
        {
            get => _users;
            set => SetProperty(ref _users, value);
        }

        private UserInfo _selectedItm;
        public UserInfo SelectedItem
        {
            get => _selectedItm;
            set => SetProperty(ref _selectedItm, value);
        }

        private string _selectedValue;
        public string SelectedValue
        {
            get => _selectedValue;
            set => SetProperty(ref _selectedValue, value);
        }
        

        private decimal _amount;
        public decimal Amount
        {
            get => _amount;
            set => SetProperty(ref _amount, value);
        }

        private async Task SendFunds()
        {
            try
            {
                IsBusy = true;
                var res = await _transactionService.CreateTransaction(new CreateTransactionRequest { Name = Name, Amount = Amount }, Properties.Settings.Default.AccessToken);
                if (res.IsSuccess)
                {
                    NotificationManager.Show("Funds sent", NotificationType.Error);
                    BackToTransactions();
                }
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                NotificationManager.Show("Something went wrong", NotificationType.Error);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task GetSuggestions()
        {
            var res2 = await _userService.GetUsersList(Properties.Settings.Default.AccessToken, new UsersListRequest { Filter = Name });
            if (res2.IsSuccess)
            {
                Users = new ObservableCollection<UserInfo>(res2.Users);
            }
        }

        private void BackToTransactions()
        {
            EventAggregator.GetEvent<ChangeViewEvent>().Publish(ViewNames.Transactions);
        }
    }
}
