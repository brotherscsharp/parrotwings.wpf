﻿using System.Windows;

namespace ParrotWings.WPF.Controls
{
    public class TextBoxControl : TextBoxControlBase
    {
        static TextBoxControl() => DefaultStyleKeyProperty.OverrideMetadata(typeof(TextBoxControl), new FrameworkPropertyMetadata(typeof(TextBoxControl)));

        protected override string InternalTextBoxName { get; } = "TextBoxEntry";

        #region LabelText

        public static readonly DependencyProperty LabelTextProperty = DependencyProperty.Register(
            nameof(LabelText), typeof(string), typeof(TextBoxControl), new PropertyMetadata(string.Empty));

        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        #endregion

        #region HelperText

        public static readonly DependencyProperty HelperTextProperty = DependencyProperty.Register(
            nameof(HelperText), typeof(string), typeof(TextBoxControl), new PropertyMetadata(string.Empty));

        public string HelperText
        {
            get => (string)GetValue(HelperTextProperty);
            set => SetValue(HelperTextProperty, value);
        }

        #endregion

        #region Required

        public static readonly DependencyProperty RequiredProperty = DependencyProperty.Register(
            nameof(Required), typeof(bool), typeof(TextBoxControl), new PropertyMetadata(default(bool)));

        public bool Required
        {
            get => (bool)GetValue(RequiredProperty);
            set => SetValue(RequiredProperty, value);
        }

        #endregion

        #region BorderHeight

        public static readonly DependencyProperty BorderHeightProperty = DependencyProperty.Register(
            nameof(BorderHeight), typeof(double), typeof(TextBoxControl), new PropertyMetadata(default(double)));

        public double BorderHeight
        {
            get => (double)GetValue(BorderHeightProperty);
            set => SetValue(BorderHeightProperty, value);
        }

        #endregion
    }
}