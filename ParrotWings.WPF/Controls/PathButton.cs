﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ParrotWings.WPF.Controls
{
    public class PathButton : Button
    {
        static PathButton() => DefaultStyleKeyProperty.OverrideMetadata(typeof(PathButton), new FrameworkPropertyMetadata(typeof(PathButton)));

        #region Data

        public static readonly DependencyProperty DataProperty = DependencyProperty.Register(nameof(Data), typeof(Geometry), typeof(PathButton), new UIPropertyMetadata(default(Geometry)));

        public Geometry Data
        {
            get => (Geometry)GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }

        #endregion

        #region PathWidth

        public static readonly DependencyProperty PathWidthProperty = DependencyProperty.Register(nameof(PathWidth), typeof(double), typeof(PathButton), new UIPropertyMetadata(double.NaN));

        public double PathWidth
        {
            get => (double)GetValue(PathWidthProperty);
            set => SetValue(PathWidthProperty, value);
        }

        #endregion

        #region PathHeight

        public static readonly DependencyProperty PathHeightProperty = DependencyProperty.Register(nameof(PathHeight), typeof(double), typeof(PathButton), new UIPropertyMetadata(double.NaN));

        public double PathHeight
        {
            get => (double)GetValue(PathHeightProperty);
            set => SetValue(PathHeightProperty, value);
        }

        #endregion

        #region PathMargin

        public static readonly DependencyProperty PathMarginProperty = DependencyProperty.Register(
            nameof(PathMargin), typeof(Thickness), typeof(PathButton), new PropertyMetadata(default(Thickness)));

        public Thickness PathMargin
        {
            get => (Thickness)GetValue(PathMarginProperty);
            set => SetValue(PathMarginProperty, value);
        }

        #endregion

        #region PathPosition

        public static readonly DependencyProperty PathPositionProperty = DependencyProperty.Register(
            nameof(PathPosition), typeof(Dock), typeof(PathButton), new PropertyMetadata(Dock.Left));

        public Dock PathPosition
        {
            get => (Dock)GetValue(PathPositionProperty);
            set => SetValue(PathPositionProperty, value);
        }

        #endregion

        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius), typeof(CornerRadius), typeof(PathButton), new PropertyMetadata(default(CornerRadius)));

        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        #endregion

        #region MouseOverBorderVisibility

        public static readonly DependencyProperty MouseOverBorderVisibilityProperty = DependencyProperty.Register(
            nameof(MouseOverBorderVisibility), typeof(Visibility), typeof(PathButton), new PropertyMetadata(default(Visibility)));

        public Visibility MouseOverBorderVisibility
        {
            get => (Visibility)GetValue(MouseOverBorderVisibilityProperty);
            set => SetValue(MouseOverBorderVisibilityProperty, value);
        }

        #endregion
    }
}