﻿using System.Windows;
using System.Windows.Controls;

namespace ParrotWings.WPF.Controls
{
    public class LoadAnimation : Control
    {
        static LoadAnimation() => DefaultStyleKeyProperty.OverrideMetadata(typeof(LoadAnimation), new FrameworkPropertyMetadata(typeof(LoadAnimation)));

        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(nameof(Size), typeof(double), typeof(LoadAnimation));

        public double Size
        {
            get => (double)GetValue(SizeProperty);
            set => SetValue(SizeProperty, value);
        }
    }
}
