﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ParrotWings.WPF.Controls
{

    public class RoundButton : Button
    {
        static RoundButton() => DefaultStyleKeyProperty.OverrideMetadata(typeof(RoundButton), new FrameworkPropertyMetadata(typeof(RoundButton)));

        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius), typeof(CornerRadius), typeof(RoundButton), new PropertyMetadata(default(CornerRadius)));

        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        #endregion

        #region OverOpacity

        public static readonly DependencyProperty OverOpacityProperty = DependencyProperty.Register(
            nameof(OverOpacity), typeof(double), typeof(RoundButton), new PropertyMetadata(0.0));

        public double OverOpacity
        {
            get => (double)GetValue(OverOpacityProperty);
            set => SetValue(OverOpacityProperty, value);
        }

        #endregion

        #region ShadowColor

        public static readonly DependencyProperty ShadowColorProperty = DependencyProperty.Register(
            nameof(ShadowColor), typeof(Color), typeof(RoundButton), new PropertyMetadata(default(Color)));

        public Color ShadowColor
        {
            get => (Color)GetValue(ShadowColorProperty);
            set => SetValue(ShadowColorProperty, value);
        }

        #endregion

        #region ShadowBackground

        public static readonly DependencyProperty ShadowBackgroundProperty = DependencyProperty.Register(
            nameof(ShadowBackground), typeof(Brush), typeof(RoundButton), new PropertyMetadata(default(Brush)));

        public Brush ShadowBackground
        {
            get => (Brush)GetValue(ShadowBackgroundProperty);
            set => SetValue(ShadowBackgroundProperty, value);
        }

        #endregion
    }
}