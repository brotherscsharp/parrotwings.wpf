﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ParrotWings.WPF.Controls
{
    public abstract class TextBoxControlBase : TextBox
    {
        private bool _forceToFocused;

        protected abstract string InternalTextBoxName { get; }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);

            if (VisualChildrenCount != 0)
                InternalTextBoxFocus();
            else
                _forceToFocused = true;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (_forceToFocused)
                InternalTextBoxFocus();
        }

        private void InternalTextBoxFocus()
        {
            ((TextBox)((Grid)GetVisualChild(0)!).FindName(InternalTextBoxName)!).Focus();
            _forceToFocused = false;
        }
    }
}