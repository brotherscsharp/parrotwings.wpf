﻿using System.Windows;
using System.Windows.Controls;

namespace ParrotWings.WPF.Controls
{
    public class PasswordBoxControl : TextBox
    {
        static PasswordBoxControl() => DefaultStyleKeyProperty.OverrideMetadata(typeof(PasswordBoxControl), new FrameworkPropertyMetadata(typeof(PasswordBoxControl)));

        #region LabelText

        public static readonly DependencyProperty LabelTextProperty = DependencyProperty.Register(
            nameof(LabelText), typeof(string), typeof(PasswordBoxControl), new PropertyMetadata("Password"));

        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        #endregion

        #region Password

        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register(
            nameof(Password), typeof(string), typeof(PasswordBoxControl), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Password
        {
            get => (string)GetValue(PasswordProperty);
            set => SetValue(PasswordProperty, value);
        }

        #endregion

        #region HelperText

        public static readonly DependencyProperty HelperTextProperty = DependencyProperty.Register(
            nameof(HelperText), typeof(string), typeof(PasswordBoxControl), new PropertyMetadata(string.Empty));

        public string HelperText
        {
            get => (string)GetValue(HelperTextProperty);
            set => SetValue(HelperTextProperty, value);
        }

        #endregion

        #region BorderHeight

        public static readonly DependencyProperty BorderHeightProperty = DependencyProperty.Register(
            nameof(BorderHeight), typeof(double), typeof(PasswordBoxControl), new PropertyMetadata(default(double)));

        public double BorderHeight
        {
            get => (double)GetValue(BorderHeightProperty);
            set => SetValue(BorderHeightProperty, value);
        }

        #endregion

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            ((Control)((Grid)GetVisualChild(0))!.FindName("PasswordBoxEntry")!).Focus();
        }
    }
}