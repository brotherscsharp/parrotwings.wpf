﻿using Newtonsoft.Json;
using Notification.Wpf;
using ParrotWings.Services;
using ParrotWings.Services.Interfaces;
using ParrotWings.WPF.Services;
using ParrotWings.WPF.Services.Interfaces;
using ParrotWings.WPF.Views;
using Prism.Ioc;
using Prism.Unity;
using Serilog;
using Serilog.Events;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ParrotWings.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private readonly ILogger _logger;
        public App()
        {


            Log.Logger = new LoggerConfiguration()
                         .MinimumLevel.Verbose()
                         .MinimumLevel.Override("Websocket.Client.WebsocketClient", LogEventLevel.Debug)
                         .CreateLogger();

            _logger = Log.Logger.ForContext<App>();
            _logger.Information("ggLeap UI started");

            _logger.Debug("Setting up global exception handling.");
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
            Dispatcher.UnhandledException += DispatcherOnUnhandledException;


            // FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement),
            //    new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
        }

        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            RegisterServices(containerRegistry);
            RegisterViews(containerRegistry);
        }

        private static void RegisterServices(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IAppService, AppService>();
            containerRegistry.Register<IAuthService, AuthService>();
            containerRegistry.RegisterSingleton<INotificationManager, NotificationManager>();
            containerRegistry.RegisterSingleton<IAuthService, AuthService>();
            containerRegistry.RegisterSingleton<IUserService, UserService>();
            containerRegistry.RegisterSingleton<ITransactionService, TransactionService>();
        }

        private static void RegisterViews(IContainerRegistry containerRegistry)
        {
            // containerRegistry.RegisterForNavigation<MainWindow>(ViewNames.MainScreen);
            containerRegistry.RegisterForNavigation<RegistrationView>(ViewNames.Registration);
            containerRegistry.RegisterForNavigation<LoginView>(ViewNames.Login);
            containerRegistry.RegisterForNavigation<HeaderView>(ViewNames.Header);
            containerRegistry.RegisterForNavigation<TransactionsView>(ViewNames.Transactions);
            containerRegistry.RegisterForNavigation<CreateTransactionView>(ViewNames.CreateTransaction);
            containerRegistry.RegisterForNavigation<UserInfoView>(ViewNames.UserInfo);
            // containerRegistry.RegisterDialog<NotificationDialog, NotificationDialogViewModel>();
        }
        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _logger.Fatal((Exception)e.ExceptionObject, "An unhandled exception occurred and was caught by the global exception handler.");
        }
        private void DispatcherOnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _logger.Fatal(e.Exception, "An unhandled exception occurred and was caught by the dispatcher exception handler.");
        }

        private void TaskSchedulerOnUnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs e)
        {
            _logger.Fatal(e.Exception, "An unhandled exception occurred and was caught by the global exception handler (TASK).");
        }

    }
}
