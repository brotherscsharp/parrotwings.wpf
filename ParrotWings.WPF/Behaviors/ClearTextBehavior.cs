﻿using Microsoft.Xaml.Behaviors;
using ParrotWings.WPF.Controls;
using System.Windows;

namespace ParrotWings.WPF.Behaviors
{
    public class ClearTextBehavior : Behavior<PathButton>
    {
        public static readonly DependencyProperty UIElementProperty = DependencyProperty.Register(
            nameof(UIElement), typeof(UIElement), typeof(ClearTextBehavior), new UIPropertyMetadata(default(UIElement)));

        public UIElement UIElement
        {
            get => (UIElement)GetValue(UIElementProperty);
            set => SetValue(UIElementProperty, value);
        }

        public static readonly DependencyProperty ElementPathProperty = DependencyProperty.Register(
            nameof(ElementPath), typeof(string), typeof(ClearTextBehavior), new PropertyMetadata("Text"));

        public string ElementPath
        {
            get => (string)GetValue(ElementPathProperty);
            set => SetValue(ElementPathProperty, value);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Click += ClearButtonOnClick;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Click -= ClearButtonOnClick;
        }

        private void ClearButtonOnClick(object sender, RoutedEventArgs e) => UIElement.GetType().GetProperty(ElementPath)?.SetValue(UIElement, null);
    }
}