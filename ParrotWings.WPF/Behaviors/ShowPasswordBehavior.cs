﻿using Microsoft.Xaml.Behaviors;
using ParrotWings.WPF.Controls;
using System.Windows;
using System.Windows.Controls;


namespace ParrotWings.WPF.Behaviors
{
    public class ShowPasswordBehavior : Behavior<PathButton>
    {
        private RoutedEventHandler _mouseDownEvent = delegate { };
        private RoutedEventHandler _mouseUpEvent = delegate { };


        public static readonly DependencyProperty PasswordBoxProperty = DependencyProperty.Register(
            nameof(PasswordBox), typeof(PasswordBox), typeof(ShowPasswordBehavior), new PropertyMetadata(default(PasswordBox)));

        public PasswordBox PasswordBox
        {
            get => (PasswordBox)GetValue(PasswordBoxProperty);
            set => SetValue(PasswordBoxProperty, value);
        }

        public static readonly DependencyProperty PasswordTextBlockProperty = DependencyProperty.Register(
            nameof(PasswordTextBlock), typeof(TextBlock), typeof(ShowPasswordBehavior), new UIPropertyMetadata(default(TextBox)));

        public TextBlock PasswordTextBlock
        {
            get => (TextBlock)GetValue(PasswordTextBlockProperty);
            set => SetValue(PasswordTextBlockProperty, value);
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            _mouseDownEvent = EyeButton_OnMouseDown;
            _mouseUpEvent = EyeButton_OnMouseUp;
            AssociatedObject.AddHandler(UIElement.MouseLeftButtonDownEvent, _mouseDownEvent, true);
            AssociatedObject.AddHandler(UIElement.MouseLeftButtonUpEvent, _mouseUpEvent, true);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonDownEvent, _mouseDownEvent);
            AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonUpEvent, _mouseUpEvent);
        }

        private void EyeButton_OnMouseDown(object sender, RoutedEventArgs routedEventArgs)
        {
            PasswordBox.Visibility = Visibility.Collapsed;
            PasswordTextBlock.Visibility = Visibility.Visible;
        }

        private void EyeButton_OnMouseUp(object sender, RoutedEventArgs routedEventArgs)
        {
            PasswordBox.Visibility = Visibility.Visible;
            PasswordTextBlock.Visibility = Visibility.Collapsed;
        }
    }
}