﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF
{
    public static class RegionNames
    {
        public const string MainContent = nameof(MainContent);
        public const string Header = nameof(Header);
        public const string UserInfo = nameof(UserInfo);
        
    }
}
