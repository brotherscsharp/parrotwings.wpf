﻿using Notification.Wpf;
using ParrotWings.WPF.Services.Interfaces;
using Prism.Events;
using Prism.Regions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF.Services
{
    public class AppService : IAppService
    {
       /* public ILogger Logger { get; }*/
        public IRegionManager RegionManager { get; }
        public IEventAggregator EventAggregator { get; }

        public INotificationManager NotificationManager { get; }

        public AppService(/*ILogger logger, */IEventAggregator eventAggregator,
                    IRegionManager regionManager,
                   INotificationManager notificationManager)
        {
            // Logger = logger;
            RegionManager = regionManager;
            EventAggregator = eventAggregator;
            NotificationManager = notificationManager;
        }
    }
}
