﻿using Notification.Wpf;
using Prism.Events;
using Prism.Regions;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF.Services.Interfaces
{
    public interface IAppService
    {
        // ILogger Logger { get; }
        IRegionManager RegionManager { get; }
        IEventAggregator EventAggregator { get; }

        INotificationManager NotificationManager { get;  }
    }
}
