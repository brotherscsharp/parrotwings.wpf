﻿using ParrotWings.WPF.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF
{
    public class ViewNames
    {
        public const string MainScreen = nameof(MainWindow);
        public const string Registration = nameof(RegistrationView);
        public const string Login = nameof(LoginView);
        public const string Transactions = nameof(TransactionsView);
        public const string CreateTransaction = nameof(CreateTransactionView);
        public const string Header = nameof(HeaderView);
        public const string UserInfo = nameof(UserInfoView);
        
    }
}
