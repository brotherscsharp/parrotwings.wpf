﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParrotWings.WPF.Helpers
{
    public class ValidationHelper
    {
        public static bool IsValidEmail(string email)
        {
            var trimmedEmail = email?.Trim();

            if (trimmedEmail == null || trimmedEmail.EndsWith("."))
            {
                return false; // suggested by @TK-421
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsValidPassword(string password)
        {
            var trimmedPassword = password?.Trim();

            if (trimmedPassword == null || trimmedPassword?.Length < 4)
            {
                return false;
            }
            return true;
        }

        public static bool IsValidUserName(string name)
        {
            var trimmedName = name?.Trim();

            if (trimmedName == null || trimmedName?.Length < 3)
            {
                return false;
            }
            return true;
        }


    }
}
