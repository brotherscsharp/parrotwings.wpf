﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ParrotWings.WPF.Converters
{
    [ValueConversion(typeof(Thickness), typeof(Thickness))]
    public class ApplyPercentagesToThicknessConverter : ConverterBase
    {
        public Thickness Percentages { get; set; }

        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            var padding = (Thickness)value!;

            return new Thickness(padding.Left * Percentages.Left, padding.Top * Percentages.Top, padding.Right * Percentages.Right, padding.Bottom * Percentages.Bottom);
        }
    }
}
