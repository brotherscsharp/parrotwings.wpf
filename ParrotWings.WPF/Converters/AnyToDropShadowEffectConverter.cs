﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace ParrotWings.WPF.Converters
{

    [ValueConversion(typeof(Brush), typeof(DropShadowEffect))]
    public class AnyToDropShadowEffectConverter : ConverterBase
    {
        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            var shadowColor = value switch
            {
                SolidColorBrush brush => brush.Color,
                Color color => color,
                _ => Colors.White
            };

            return CreateDropShadowEffect(shadowColor);
        }

        private static DropShadowEffect CreateDropShadowEffect(Color color) => new DropShadowEffect
        {
            Color = color,
            Direction = 0,
            BlurRadius = 24,
            ShadowDepth = 0
        };
    }
}
