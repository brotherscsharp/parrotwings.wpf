﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ParrotWings.WPF.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BooleanToVisibilityConverter : ConverterBase
    {
        public bool Inverse { get; set; }

        public Visibility InvisibleValue { get; set; } = Visibility.Collapsed;

        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = (bool)value!;

            if (Inverse)
                boolValue = !boolValue;

            return boolValue ? Visibility.Visible : InvisibleValue;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility)value;

            if (Inverse)
                return visibility == InvisibleValue;

            return visibility == Visibility.Visible;
        }
    }
}
