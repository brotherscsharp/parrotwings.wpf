﻿using System;
using System.Globalization;


namespace ParrotWings.WPF.Converters
{
    public class DoubleDivideConverter : ConverterBase
    {
        public double Divider { get; set; }

        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture) =>
            (double?)value / Divider ?? 0;
    }
}
