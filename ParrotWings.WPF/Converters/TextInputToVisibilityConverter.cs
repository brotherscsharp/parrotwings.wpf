﻿using System;
using System.Globalization;
using System.Windows;

namespace ParrotWings.WPF.Converters
{
    public class TextInputToVisibilityConverter : MultiConverterBase
    {
        public bool Inverse { get; set; }

        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is bool hasFocus && hasFocus || values[1] is int length && length != 0)
                return Inverse ? Visibility.Visible : Visibility.Collapsed;

            return Inverse ? Visibility.Collapsed : Visibility.Visible;
        }
    }
}
