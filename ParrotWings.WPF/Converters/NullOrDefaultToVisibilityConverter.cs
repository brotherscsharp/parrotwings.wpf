﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ParrotWings.WPF.Converters
{
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class NullOrDefaultToVisibilityConverter : ConverterBase
    {
        public bool Inverse { get; set; }

        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            var nullOrDefault = false;

            if (value == null)
                nullOrDefault = true;
            else
            {
                if (value is int intValue)
                    nullOrDefault = intValue == 0;
                else if (value is TimeSpan timeSpan)
                    nullOrDefault = timeSpan == TimeSpan.Zero;
            }

            if (Inverse)
                nullOrDefault = !nullOrDefault;

            return nullOrDefault ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}