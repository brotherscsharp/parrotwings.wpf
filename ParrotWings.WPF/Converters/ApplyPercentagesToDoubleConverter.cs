﻿using System;
using System.Globalization;

namespace ParrotWings.WPF.Converters
{
    public class ApplyPercentagesToDoubleConverter : ConverterBase
    {
        public int Percent { get; set; }

        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture) => (double)value! * Percent / 100;
    }
}
