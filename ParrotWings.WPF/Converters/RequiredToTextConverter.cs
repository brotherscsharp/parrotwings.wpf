﻿using System;
using System.Globalization;
using System.Windows.Data;


namespace ParrotWings.WPF.Converters
{
    [ValueConversion(typeof(bool), typeof(string))]
    public class RequiredToTextConverter : ConverterBase
    {
        public override object Convert(object? value, Type targetType, object parameter, CultureInfo culture) => (bool)value! ? "*" : string.Empty;
    }
}
