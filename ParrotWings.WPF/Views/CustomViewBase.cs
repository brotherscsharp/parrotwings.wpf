﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ParrotWings.WPF.Views
{
    public class CustomViewBase : UserControl
    {
        public CustomViewBase()
        {
            // Find a better way to specify default style because DefaultStyleKeyProperty.OverrideMetadata does not seem to work
            Style = Application.Current.Resources["DefaultCustomViewBaseStyle"] as Style;
            PreviewMouseDown += OnPreviewMouseDown;
            PreviewMouseUp += OnPreviewMouseDown;
        }

        private void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (IsBusy)
                e.Handled = true;
        }

        public bool IsBusy
        {
            get => (bool)GetValue(IsBusyProperty);
            set { SetValue(IsBusyProperty, true); }
        }

        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(CustomViewBase), new PropertyMetadata(false));
    }
}
