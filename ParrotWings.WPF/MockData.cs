﻿using ParrotWings.Models.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ParrotWings.WPF
{
    public class MockData
    {
        public static string AppName = "parrot wings";
        public static string UserName = "Servin5";
        public static string Email = "servin5@sys.im";
        public static string Password = "123456";

        public static FullUserInfo UserInfo = new FullUserInfo
        {
            Balance = 500,
            Email = "servin@sys.im",
            Name = "Servin"
        };

        public static ObservableCollection<Transaction> Transactions = new ObservableCollection<Transaction>
        {
            new Transaction() { UserName = "User 1", Amount = 4999, Balance = 500, Date= new DateTime() },
            new Transaction() { UserName = "User 2", Amount = 23, Balance = 222, Date= new DateTime() },
            new Transaction() { UserName = "User 3", Amount = 34, Balance = 3, Date= new DateTime() },
            new Transaction() { UserName = "User 1", Amount = 4999, Balance = 33333, Date= new DateTime() },
            new Transaction() { UserName = "User 1", Amount = 33, Balance = 232, Date= new DateTime() },
            new Transaction() { UserName = "User 4", Amount = 3, Balance = 500, Date= new DateTime() },
            new Transaction() { UserName = "User 2", Amount = 4, Balance = 500, Date= new DateTime() },
            new Transaction() { UserName = "User 5", Amount = 343, Balance = 3434, Date= new DateTime() },
            new Transaction() { UserName = "User 2", Amount = 1, Balance = 500, Date= new DateTime() },
            new Transaction() { UserName = "User 4", Amount = 23, Balance = 500, Date= new DateTime() },
            new Transaction() { UserName = "User 2", Amount = 36, Balance = 500, Date= new DateTime() },
        };

        public static ObservableCollection<string> Usernames= new ObservableCollection<string>
        {
            "user 2",
            "3user 12",
            "5user 22",
            "5user 32",
            "5user 42",
            "6user 52",
            "user 52",
        };

        public static ObservableCollection<UserInfo> Users = new ObservableCollection< UserInfo >
        {
            new UserInfo() { Name = "User 1", Id = "1" },
            new UserInfo() { Name = "Uqser 12", Id = "13" },
            new UserInfo() { Name = "Uwser 13", Id = "14" },
            new UserInfo() { Name = "Ueser 14", Id = "15" },
            new UserInfo() { Name = "Urser 15", Id = "16" },
            new UserInfo() { Name = "Utser 16", Id = "17" },
            new UserInfo() { Name = "Uyser 16", Id = "18" },
            new UserInfo() { Name = "User 17", Id = "19" },
            new UserInfo() { Name = "User 1", Id = "10" },
        };
        
    }
}
